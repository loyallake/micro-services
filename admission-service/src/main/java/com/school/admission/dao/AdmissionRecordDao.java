package com.school.admission.dao;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.school.admission.model.AdmissionRecord;

@Repository
public class AdmissionRecordDao extends BaseDao<AdmissionRecord>{
	
	@Override
	protected Class<AdmissionRecord> getDomainClass() {
		return AdmissionRecord.class;
	}

	public AdmissionRecord findByAdmissionNumber(String admissionNumber) {
		Query query = new Query();
		query.addCriteria(Criteria.where("admissionNumber").is(admissionNumber));
		List<AdmissionRecord> list = getMongoTemplate().find(query, AdmissionRecord.class);
		if(!CollectionUtils.isEmpty(list)) {
			return list.iterator().next();
		}
		 
		return null;
	}
}
