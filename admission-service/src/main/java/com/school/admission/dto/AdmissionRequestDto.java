package com.school.admission.dto;

import java.util.List;

import javax.validation.Valid;

import com.school.admission.model.AdmissionRecord;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdmissionRequestDto {
	
	@Valid
	private List<AdmissionRecord> admissionRecords;

}
