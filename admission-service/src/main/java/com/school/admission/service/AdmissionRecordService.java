package com.school.admission.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.school.admission.dao.AdmissionRecordDao;
import com.school.admission.exception.ApplicationException;
import com.school.admission.model.AdmissionRecord;

@Service
public class AdmissionRecordService {
	
	@Autowired
	AdmissionRecordDao admissionRecordDao;
	
	public List<AdmissionRecord> findAll() {
		return admissionRecordDao.findAll();
	}
	
	public void saveAll(List<AdmissionRecord> admissionRecords) throws ApplicationException {
		admissionRecordDao.insertAll(admissionRecords);
	}

	public AdmissionRecord findByAdmissionNumber(String admissionNumber) {
		return admissionRecordDao.findByAdmissionNumber(admissionNumber);
	}
	
	public void update(AdmissionRecord admissionRecord) throws ApplicationException {
		
		if(this.findByAdmissionNumber(admissionRecord.getAdmissionNumber()) == null) {
			throw new ApplicationException("Record not found");
		}
		
		admissionRecordDao.update(admissionRecord);
	}
	
}
