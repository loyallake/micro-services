package com.school.documentstorage.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

@Configuration
public class AmazonClientConfig {
	
	@Autowired
	AppProperties appProperties;

	@Bean
	public AmazonS3 setAmazonClient() {
		return AmazonS3ClientBuilder.defaultClient();
	}
	
}
