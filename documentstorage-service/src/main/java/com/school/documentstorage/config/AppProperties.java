package com.school.documentstorage.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Component
@ConfigurationProperties(prefix="app")
@Getter
@Setter
@ToString
public class AppProperties {
	
	private aws aws;
	
	@Getter
	@Setter
	public static class aws {
		private s3 s3;
	}
	
	@Getter
	@Setter
	public static class s3 {
		private String bucket;
		private String region;
		private String folder;
	}
	
}
