package com.school.documentstorage.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.school.documentstorage.model.DocumentModel;
import com.school.documentstorage.service.DocumentService;

@RestController
@RequestMapping("document/")
public class DocumentController {
	
	@Autowired
	DocumentService documentService;
	
	@PutMapping("/uploadFile/{admissionNumber}")
    public DocumentModel uploadFile(@RequestPart(value = "file") MultipartFile file, @PathVariable("admissionNumber") String admissionNumber) throws IOException {
		//validate admission number
		DocumentModel documentModel = documentService.upload(admissionNumber, file);
        return documentModel;
    }

}
