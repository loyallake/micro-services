package com.school.documentstorage.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Document(collection="document")
@Data
public class DocumentModel {

	@Id
	private String id;
	
    
	private String mimeType;
	
	private String documentName;
	
	private String admissionNumber;
	
	private String versionId;
    
    private String preSignedS3Url;
    
    @JsonIgnore
    private String fileVersionId;
    
    @JsonIgnore
    private String documentBucketName;
    
    @JsonIgnore
    private String directory;
	
}
