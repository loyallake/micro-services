package com.school.documentstorage.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.school.documentstorage.model.DocumentModel;

@Repository
public interface DocumentRepository extends MongoRepository<DocumentModel, String> {


}
