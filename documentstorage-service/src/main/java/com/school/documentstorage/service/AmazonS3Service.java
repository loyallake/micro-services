package com.school.documentstorage.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.school.documentstorage.model.DocumentModel;

@Service
public class AmazonS3Service {
	
	@Autowired
	AmazonS3 amazonS3;
	
	public String upload(DocumentModel documentModel, MultipartFile multipartFile) throws IOException {
		
		  String key = String.join("/", documentModel.getDirectory(), documentModel.getId());
		  
		  ObjectMetadata objectMetadata = new ObjectMetadata();
		  objectMetadata.setContentType(documentModel.getMimeType());
		  objectMetadata.setContentLength(multipartFile.getSize());
		  PutObjectResult putObjectResult = amazonS3.putObject(new PutObjectRequest(documentModel.getDocumentBucketName(), key, multipartFile.getInputStream(), objectMetadata));
		  if(putObjectResult != null) {
			  return getPresignedUrl(documentModel);
		  }
		return StringUtils.EMPTY;
		 
	}
	
	public void get(DocumentModel documentModel) {
		
	}
	
	public String getPresignedUrl(DocumentModel documentModel) {
		  String key = String.join("/", documentModel.getDirectory(), documentModel.getId());

		  // Set the presigned URL to expire after one hour.
        java.util.Date expiration = new java.util.Date();
        long expTimeMillis = expiration.getTime();
        expTimeMillis += 1000 * 60 * 60;
        expiration.setTime(expTimeMillis);

        // Generate the presigned URL.
        System.out.println("Generating pre-signed URL.");
        GeneratePresignedUrlRequest generatePresignedUrlRequest = 
                new GeneratePresignedUrlRequest(documentModel.getDocumentBucketName(), key)
                .withMethod(HttpMethod.GET)
                .withExpiration(expiration);
        URL url = amazonS3.generatePresignedUrl(generatePresignedUrlRequest);
        
        return url.toString();
	}

}
