package com.school.documentstorage.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.school.documentstorage.config.AppProperties;
import com.school.documentstorage.model.DocumentModel;
import com.school.documentstorage.repository.DocumentRepository;

@Service
public class DocumentService {
	
	@Autowired
	DocumentRepository documentRepository;
	
	@Autowired
	AmazonS3Service amazonS3Service;
	
	@Autowired
	AppProperties appProperties;

	public DocumentModel upload(String admissionNumber, MultipartFile multipartFile) { 
		DocumentModel documentModel = new DocumentModel();
		documentModel.setDocumentName(multipartFile.getOriginalFilename());
		documentModel.setAdmissionNumber(admissionNumber);
		documentModel.setDirectory(admissionNumber);
		documentModel.setMimeType(multipartFile.getContentType());
		documentModel.setDirectory(admissionNumber);
		documentModel.setDocumentBucketName(appProperties.getAws().getS3().getBucket());
		documentRepository.save(documentModel);
		try {
			String url = amazonS3Service.upload(documentModel, multipartFile);
			documentModel.setPreSignedS3Url(url);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return documentModel;
	}
}
